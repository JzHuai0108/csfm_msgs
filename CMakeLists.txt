cmake_minimum_required(VERSION 2.8.3)
project(csfm_msgs)
find_package(catkin REQUIRED COMPONENTS std_msgs sensor_msgs message_generation)

add_message_files(
  DIRECTORY msg
  FILES
  cvPoint2f.msg
  cvKeyPoint.msg
  cvCameraModel.msg
  AddMapPoint.msg
  AddKeyFrame.msg
  AddKeyFrameAndImuData.msg
  Camera.msg
  Frame.msg
  NeighborConstraint.msg
  QueryMultiFrame.msg
)

generate_messages(
  DEPENDENCIES
  sensor_msgs
  std_msgs
)

catkin_package(CATKIN_DEPENDS message_runtime std_msgs sensor_msgs)
