# README #

csfm_msgs

### What is this repository for? ###

* csfm_msgs are used by several packages for ROS communication. It is used as library by these packages.
* 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

make sure you have a ROS distribution installed, e.g., ROS indigo.

create a ROS workspace, e.g., $HOME/catkin_ws, following ROS tutorial online.

        cd catkin_ws/src
        git clone https://JzHuai0108@bitbucket.org/JzHuai0108/csfm_msgs.git
        cd
        cd catkin_ws
        catkin build -DCMAKE_BUILD_TYPE=Release --pkg csfm_msgs ..


* Dependencies

ROS indigo and very likely other ROS distributions.

* Deployment instructions

If the package to link against **csfm_msgs** is also a ROS package, you need to declare **csfm_msgs** in its cmakelists.txt via the command

        FIND_PACKAGE(catkin REQUIRED COMPONENTS csfm_msgs)
            catkin_package(   
            CATKIN_DEPENDS csfm_msgs
        )

Otherwise, I doubt that that package needs csfm_msgs.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
